class BadRequestError extends Error {
    constructor(message = 'Client error') {
        super(message);
        this.name = 'BadRequestError';
        this.code = 400;
    }
}

class ServerError extends Error {
    constructor(message = 'Server error') {
        super(message);
        this.name = 'ServerError';
        this.code = 500;
    }
}

module.exports = { BadRequestError, ServerError };
