const allowedExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

module.exports = { allowedExtensions };
