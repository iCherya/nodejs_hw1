const express = require('express');
const router = express.Router();
const path = require('path');

const { BadRequestError, ServerError } = require('../errors/apiErrors');

const File = require('../model/File');
const Files = require('../model/Files');

const filesPath = path.resolve(__dirname, '..', 'files');
const files = new Files(filesPath);

router.post('/', (req, res, next) => {
    const errorMessage = File.validateFileCreationPayload(req.body);

    if (errorMessage) {
        return next(new BadRequestError(errorMessage));
    }

    const file = new File(req.body);
    file.addToFolder(filesPath)
        .then(() =>
            res.status(200).json({
                message: 'File created successfully'
            })
        )
        .catch((error) => next(new ServerError()));
});

router.get('/', (req, res, next) => {
    files
        .getData()
        .then((data) => res.status(200).json({ message: 'Success', files: data }))
        .catch((error) => next(new ServerError()));
});

router.get('/:filename', (req, res, next) => {
    files
        .find(req.params.filename)
        .then((file) => {
            return res.status(200).json(file);
        })
        .catch((error) => {
            if (error instanceof BadRequestError) {
                return next(error);
            }

            return next(new ServerError());
        });
});

router.use((req, res, next) => {
    next(new BadRequestError());
});

router.use('/', (err, req, res, next) => {
    res.status(err.code).json({ message: err.message });
});

module.exports = router;
