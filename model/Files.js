const fs = require('fs').promises;
const File = require('./File');
const { BadRequestError } = require('../errors/apiErrors');

class Folder {
    constructor(folderPath) {
        this.folderPath = folderPath;

        this.checkForExistingFolder();
    }
    checkForExistingFolder() {
        fs.access(this.folderPath).catch((err) => {
            return fs.mkdir(this.folderPath);
        });
    }
    getData() {
        return fs.readdir(this.folderPath);
    }
    find(filename) {
        const filePath = `${this.folderPath}/${filename}`;
        let file;

        return this.getData()
            .then((files) => {
                if (!files.find((el) => el === filename)) {
                    throw new BadRequestError(`No file with ${filename} filename found`);
                }

                return fs.readFile(filePath, 'utf-8');
            })
            .then((content) => {
                file = new File({ filename, content });
                file.extension = filename.split('.').pop();
                file.message = 'Success';

                return fs.stat(filePath, 'utf-8');
            })
            .then((stat) => {
                file.uploadedDate = stat.ctime;

                return file;
            });
    }
}

module.exports = Folder;
