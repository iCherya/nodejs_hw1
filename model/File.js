const fs = require('fs').promises;
const { allowedExtensions } = require('../constants/constants');

class File {
    constructor({ filename, content }) {
        this.filename = filename;
        this.content = content;
    }

    static validateFileCreationPayload({ filename, content }) {
        if (!filename) {
            return `Please specify 'filename' parameter`;
        }

        if (!content) {
            return `Please specify 'content' parameter`;
        }

        if (!filename.includes('.')) {
            return `Filename may contain '.' symbol`;
        }

        const extention = filename.split('.').pop();
        if (!allowedExtensions.includes(extention)) {
            return `Unsupported file extension`;
        }

        return null;
    }

    addToFolder(folderPath) {
        this.folderPath = folderPath;

        return fs.writeFile(`${folderPath}/${this.filename}`, this.content);
    }
}

module.exports = File;
