const express = require('express');
const morgan = require('morgan');
const fileRouter = require('./routes/fileRouter');

const app = express();
const port = 8080;

app.use(express.json());
app.use(morgan('combined'));

app.use('/api/files', fileRouter);

app.use((req, res) => {
    res.status(404).json({ message: 'Not found' });
});

app.listen(port, () => {
    console.log(`Server started at port ${port}`);
});
